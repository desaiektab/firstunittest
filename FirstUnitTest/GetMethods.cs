﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace FirstUnitTest
{
    class GetMethods
    {
        /// <summary>
        /// Returns the title of webpage
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static string GetTitle(IWebDriver driver)
        {
            return driver.Title;
        }
        public static string GetText(IWebElement element)
        {
             return element.Text;
            //return element.GetAttribute("value");
        }

        public static string GetSelectedTextFromDDL(IWebElement element)
        {
            return new SelectElement(element).AllSelectedOptions.SingleOrDefault().Text;
        }
    }
}
