﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace FirstUnitTest
{
    internal class LoginPage : BasePage
    {
        String url = "https://qaio.workforcelogiq.com/zcw/Login/Login?TabId=1";
        String userName = "dtong@workforcelogiq.com";
        String password = "Joker@32";

        //public LoginPage(IWebDriver driver) : base(driver)
        //{
        //       // that means when it comes to child class constructor, it will first execute parent constructor, so no need to write code here
        //    //hence we are writting the code in single block
        //}
       public LoginPage(IWebDriver driver) : base(driver) { } //--> Instead of above block, we need to write this single line block

        public void LoginToApplication()
        {
            driver.Navigate().GoToUrl(url);

            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
            
            driver.FindElement(By.XPath("//input[@id='Username']")).SendKeys(userName);
            driver.FindElement(By.XPath("//input[@type='password']")).SendKeys(password);

            driver.FindElement(By.CssSelector("button.btn.check-submit")).Click(); // login button click

            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            var DashboardElement =  wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(@href,'Dashboard')]")));
           
            Assert.IsTrue(DashboardElement.Displayed, "Login Failed..");
        }
        public void TearDown()
        {
            Thread.Sleep(19000);
            driver.Quit();
        }
    }
}
