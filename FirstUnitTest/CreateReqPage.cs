﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace FirstUnitTest
{
    internal class CreateReqPage: BasePage
    {
        public CreateReqPage(IWebDriver driver) : base(driver)
        {
            // it will call the base class constructor
        }
        internal void NavigateToCreateReqPageFromMenu()
        {
            IWebElement reqmMenu = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("li.icon-requisition.parent-nav")));
            action.MoveToElement(reqmMenu).Perform();

            IWebElement createRequMenu = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(@href, 'CreateRequisition')]")));
            action.MoveToElement(createRequMenu).Click().Build().Perform();
             wait.Until(ExpectedConditions.UrlContains("CreateRequisition"));

          //  Assert.IsTrue(driver.FindElement(By.CssSelector("div.section-title")).Displayed, "Unable to Navigate to Create Requisition Page");
            Assert.AreEqual("Add Requisition", driver.Title, "Unable to Navigate to Create Requisition Page"); 
        }

        internal void CreateRequistionSelection()
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("DivisionsDropdown")));

            SelectElement divison = new SelectElement(driver.FindElement(By.Id("DivisionsDropdown")));
            divison.SelectByText("Cox Communications");

            IWebElement Wizardque = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("label.zc-radio")));
           
       //Selecting wizard question
            IList<IWebElement> wizardque = driver.FindElements(By.CssSelector("label.zc-radio"));

            foreach (IWebElement LabelElement in wizardque)
            {
                IWebElement SpanElement = LabelElement.FindElement(By.TagName("span"));
       
                if (SpanElement.Text.Contains("Construction"))
                {
                    LabelElement.Click();
                    break;
                }
            }
       // Need click on 'Create New' Or Create from Recent req section		
            IList<IWebElement> createReqSelection = driver.FindElements(By.XPath("//label[@class='zc-radio inline']"));

            foreach (IWebElement reqElement in createReqSelection) 
            {
                String CreateButtonName = reqElement.FindElement(By.TagName("span")).Text;
                if (CreateButtonName.Equals("Create New")) 
                {
                    reqElement.Click();
                }
            }
      // Select classification

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("JobClassificationDropdown")));
            SelectElement classification = new SelectElement(driver.FindElement(By.Id("JobClassificationDropdown")));
            classification.SelectByText("Construction Services - Arizona (CS)");

      //Clicking on Create New button		
            driver.FindElement(By.Id("NewRequisition")).Click();

            wait.Until(ExpectedConditions.UrlContains("SOWRequisitionDetails")); // Waiting till page loads
         
            Assert.AreEqual("Statement Of Work Requisition", driver.Title, "Unable to navigate to Requisition ENTRY page");
        }
    }
}