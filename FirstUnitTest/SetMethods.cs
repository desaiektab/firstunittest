﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace FirstUnitTest
{
    // Will be using - Extended methods so made class as 'static'. Exetended methods concept is only for C#
    public static class SetMethods
    {
        /// <summary>
        /// Extended method for Entering text in the control
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>

        public static void EnterText(this IWebElement element, string value) 
        {
            element.SendKeys(value);
        }

        /// <summary>
        /// Extended method to click on button, checkbox, radio button
        /// </summary>
        /// <param name="element"></param>
        public static void ClickOnElement(this IWebElement element)
        {
            element.Click();
        }







    }
}
