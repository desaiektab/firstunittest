﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace FirstUnitTest
{
    public class BasePage
    {
        protected IWebDriver driver;
        protected Actions action;
        protected WebDriverWait wait;
        protected IJavaScriptExecutor js;
        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            action = new Actions(driver);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            js = (IJavaScriptExecutor)driver;
        }
    }
}