﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace FirstUnitTest
{
    internal class ReqEntryPage: BasePage
    {
        public ReqEntryPage(IWebDriver driver) : base(driver) { }

        internal void FillCreateRequistionData()
        {
			//Create req page		
			IWebElement reqName = wait.Until(ExpectedConditions.ElementToBeClickable(driver.FindElement(By.Id("ReqName"))));
		    reqName.SendKeys("ED_req_01");

			// BU SelectElemention

					IWebElement BU = driver.FindElement(By.CssSelector("a[class='chzn-single']"));
					BU.Click();

			// BU - autoType dropdown
			wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("[id='OrgLevelID_chzn'] [class='chzn-search']")));
					
			IWebElement BU_autoType = driver.FindElement(By.CssSelector("[id='OrgLevelID_chzn'] [class='chzn-search']"));
					BU_autoType.Click();
					BU_autoType.SendKeys("Cox Communications");

			string BUname = (String)js.ExecuteScript("return document.getElementsByClassName(\"chzn-search\").value;");

			while (!BUname.Equals("Cox Communications"))
			{
				BU_autoType.SendKeys(Keys.Down);
				BUname = (String)js.ExecuteScript("return document.getElementsByClassName(\"chzn-search\").value;");
			}
			BU_autoType.SendKeys(Keys.Enter);

	// Postal code - selection		
						
			IWebElement zip = driver.FindElement(By.XPath("//input[@id='Zip']"));
				zip.SendKeys("123");
			Thread.Sleep(1000);
				zip.SendKeys(Keys.Down);
				zip.SendKeys(Keys.Enter); //--> Need to figure out postal code --> using javascript executor

			//   ------- If we want to select particular 'Postal code' then use below code of javascript executor: Above code will select first postal code
			//IWebElement zip = driver.FindElement(By.XPath("//input[@id='Zip']"));
			//zip.SendKeys("123");

			//String zipText = (String)js.ExecuteScript("return document.getElementById(\"ManagersDropdown_chzn\").value;");

			//while (!zipText.Contains("12302"))
			//{
			//	zip.SendKeys(Keys.Down);
			//	Thread.Sleep(1000);
			//	zipText = (String)js.ExecuteScript("return document.getElementById(\"Zip\").value;");

			//}
			//zip.SendKeys(Keys.Enter);

			//   ----------- Note: Country, city field values are fetching from Postal code. so I have commented below code:

			//SelectElement country = new SelectElement(driver.FindElement(By.XPath("//select[@id='CountryCode']")));
			//country.SelectByText("United States");

			//SelectElement state = new SelectElement(driver.FindElement(By.XPath("//select[@id='StateRecID']")));
			//state.SelectByText("Alaska");

			IWebElement budget = driver.FindElement(By.CssSelector("input#EstimatedCost"));
			budget.Click();
			int budgetValue = 5000;
			budget.SendKeys(" "+budgetValue);

			SelectElement currency = new SelectElement(driver.FindElement(By.CssSelector("select[id='ReqCurrencyID']")));
			currency.SelectByText("US Dollars");

			driver.FindElement(By.CssSelector("textarea[id='EngagementDescription']")).SendKeys("test description");

// SelectElementing start date - as Today's date

			IWebElement reqStartDate = driver.FindElement(By.CssSelector("input[id='ReqStartDate']"));
			reqStartDate.Click();
			
			var todayDate = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("td.active.day")));
			todayDate.Click(); // SelectElementing start date as today's date

			IWebElement reqEndDate = driver.FindElement(By.CssSelector("input[id='ReqEndDate']"));
			reqEndDate.Click();
// Select end date different than today's date

			IWebElement monthName = driver.FindElement(By.CssSelector("[class ='datepicker-days'] [class='datepicker-switch']"));
			IWebElement nextButton = driver.FindElement(By.CssSelector("[class='datepicker-days'] [class='next']"));


			// Year & Month selection - common code to select month and year
			while (!monthName.Text.Contains("January 2021"))
			{
				nextButton.Click();
			}
			//Date selection - common code to select date
			IList<IWebElement> dates = driver.FindElements(By.CssSelector("[class='day']"));

			foreach(IWebElement dateElement in dates)
			{
				if (dateElement.Text.Contains("25"))
				{
					dateElement.Click();
					break;
				}
			}
			// Manager selection --> Using javascript executor

			IWebElement managerSelection = driver.FindElement(By.Id("ManagersDropdown_chzn"));
			managerSelection.SendKeys("ras");

			string managerName = (String)js.ExecuteScript("return document.getElementById(\"ManagersDropdown_chzn\").value;");

			while (!managerName.Equals("Rashmi Sinha")) 
			{
				managerSelection.SendKeys(Keys.Down);
				managerName = (String)js.ExecuteScript("return document.getElementById(\"ManagersDropdown_chzn\").value;");
			}
			managerSelection.SendKeys(Keys.Enter);

			// Clicking on 'Add' button

			IWebElement addbutton = driver.FindElement(By.Id("ROAddManagerAnchor"));
			addbutton.Click();


			// Clicking on 'Save' button

			IWebElement saveButton = driver.FindElement(By.Id("SaveButton"));
			saveButton.Click();

			
		}
	}
}