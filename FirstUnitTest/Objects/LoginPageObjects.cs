﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace FirstUnitTest
{
    class LoginPageObjects
    {
        public LoginPageObjects(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "Username")]
        public IWebElement txtUserName;

        [FindsBy(How = How.XPath, Using = "//input[@type='password']")]
        public IWebElement txtPassword;

        [FindsBy(How = How.CssSelector, Using = "button.btn.check-submit")]
        public IWebElement btnSubmit;

        public void LoginToApp(string userName, string password)
        {
            
            txtUserName.EnterText(userName);
            txtPassword.EnterText(password);
            btnSubmit.ClickOnElement();

            //return new LoginObject();
        }


    } 
}
